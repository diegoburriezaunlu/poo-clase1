package ar.edu.UNLU.POO.clase1;

public class Pila {
    private Nodo tope = null;

    public void apilar (Object dato) {
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        Nodo nodoAux = tope;
        if (nodoAux == null) {
            tope = nodoNuevo;
        } else {
            tope = nodoNuevo;
            tope.setProximo(nodoAux);
        }
    }

    public Object desapilar (){
        if (tope == null){
            return "Esta vacia";
        } else {
            Object respuesta = tope.getDato();
            tope = tope.getProximo();
            return respuesta;
        }
    }

    public boolean esVacia (){
        if (tope == null) {
            return true;
        } else {
            return false;
        }
    }

    public Object recuperarTope (){
        if(tope == null){
            return "";
        }else{
            return tope.getDato();
        }
    }
}
